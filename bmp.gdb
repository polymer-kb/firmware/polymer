target extended-remote /dev/ttyACM0
monitor swdp_scan
attach 1

load

set print asm-demangle on

break DefaultHandler
break UserHardFault
break rust_begin_unwind
