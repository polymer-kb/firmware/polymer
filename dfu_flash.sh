#!/usr/bin/env bash

set -euo pipefail

cd "${0%/*}"

cargo objcopy --bin polymer --release -- -O binary build/polymer.bin

sudo dfu-util -a 2 -d 1209:9200 -D build/polymer.bin
