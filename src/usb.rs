use core::pin::Pin;
use futures::{
    prelude::*,
    task::{
        Context,
        Poll,
    },
};
use keebrs::{
    led::LedReport,
    translate::Translated,
    usb::{
        hid::HidClass,
        keyboard::Keyboard,
    },
};
use usb_device::bus::UsbBus;

pub struct UsbSink<C>(pub C);

impl<C, B: UsbBus + 'static, L: LedReport> Sink<Translated> for UsbSink<C>
where
    C: rtic::Mutex<T = HidClass<'static, B, Keyboard<L>>> + Unpin,
{
    type Error = ();
    fn start_send(self: Pin<&mut Self>, item: Translated) -> Result<(), ()> {
        self.get_mut().0.lock(|class| {
            class.get_device().update_report(item);
            let _ = class.write_report();
            Ok(())
        })
    }
    fn poll_ready(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), ()>> {
        Poll::Ready(Ok(()))
    }
    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), ()>> {
        Poll::Ready(Ok(()))
    }
    fn poll_close(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), ()>> {
        Poll::Ready(Ok(()))
    }
}
