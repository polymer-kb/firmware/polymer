use core::sync::atomic::{
    AtomicUsize,
    Ordering,
};

use lock_api::{
    GuardNoSend,
    RawMutex,
};

const ACTIVE: usize = usize::max_value() << (usize::max_value().count_ones() / 2);

/// [lock_api::RawMutex] implementation that disables interrupts.
///
/// As with [cortex_m::interrupt::free], it will not re-enable interrupts on
/// `unlock` if they were disabled already when `lock` was called.
pub struct IFree(AtomicUsize);

unsafe impl RawMutex for IFree {
    type GuardMarker = GuardNoSend;
    const INIT: IFree = IFree(AtomicUsize::new(0));

    fn lock(&self) {
        let primask = cortex_m::register::primask::read();
        cortex_m::interrupt::disable();
        if primask.is_active() {
            self.0.swap(ACTIVE, Ordering::Acquire);
        } else {
            self.0.fetch_add(1, Ordering::Acquire);
        }
    }

    unsafe fn unlock(&self) {
        if self.0.fetch_sub(1, Ordering::AcqRel) == ACTIVE {
            self.0.store(0, Ordering::Release);
            cortex_m::interrupt::enable();
        }
    }

    fn try_lock(&self) -> bool {
        self.lock();
        true
    }
}
