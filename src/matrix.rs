use keebrs::{
    debounce::DebounceState,
    matrix::{
        Matrix,
        Pull,
        Read,
    },
    stateful,
};
use stm32f1xx_futures::{
    self,
    hal::gpio::{
        gpioa::{
            PA0,
            PA3,
            PA10,
            PA6,
            PA7,
            PA8,
            PA9,
        },
        gpiob::{
            PB0,
            PB5,
            PB8,
        },
        gpioc::{
            PC14,
            PC15,
        },
        Input,
        Output,
        PullUp,
        PushPull,
    },
};

pub type Scanner = Matrix<
    (
        PB5<Output<PushPull>>,
        PB8<Output<PushPull>>,
        PA0<Output<PushPull>>,
        PC15<Output<PushPull>>,
        PC14<Output<PushPull>>,
    ),
    (
        PA8<Input<PullUp>>,
        PA10<Input<PullUp>>,
        PA9<Input<PullUp>>,
        PA7<Input<PullUp>>,
        PA6<Input<PullUp>>,
        PB0<Input<PullUp>>,
        PA3<Input<PullUp>>,
    ),
>;

pub type StatefulScanner<S, D> =
    stateful::StatefulScanner<S, [[DebounceState; Scanner::NREAD]; Scanner::NPULL], D>;

macro_rules! build_scanner {
    ($afio:expr, $gpioa:expr, $gpiob:expr, $gpioc:expr) => {{
        // #[allow(unused_variables)]
        // let (pa15, pb3, pb4) = $afio.mapr.disable_jtag($gpioa.pa15, $gpiob.pb3, $gpiob.pb4);
        Matrix {
            pull: (
                $gpiob.pb5.into_push_pull_output(&mut $gpiob.crl),
                $gpiob.pb8.into_push_pull_output(&mut $gpiob.crh),
                $gpioa.pa0.into_push_pull_output(&mut $gpioa.crl),
                $gpioc.pc15.into_push_pull_output(&mut $gpioc.crh),
                $gpioc.pc14.into_push_pull_output(&mut $gpioc.crh),
            ),
            read: (
                $gpioa.pa8.into_pull_up_input(&mut $gpioa.crh),
                $gpioa.pa10.into_pull_up_input(&mut $gpioa.crh),
                $gpioa.pa9.into_pull_up_input(&mut $gpioa.crh),
                $gpioa.pa7.into_pull_up_input(&mut $gpioa.crl),
                $gpioa.pa6.into_pull_up_input(&mut $gpioa.crl),
                $gpiob.pb0.into_pull_up_input(&mut $gpiob.crl),
                $gpioa.pa3.into_pull_up_input(&mut $gpioa.crl),
            ),
        }
    }};
}
