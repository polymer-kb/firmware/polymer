{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        toolchain = (pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain).override {
          extensions = [ "llvm-tools-preview" ];
          targets = [ "thumbv7m-none-eabi" ];
        };
      in
      with pkgs;
      {
        devShells.default = mkShell {
          buildInputs = [
            toolchain
            dfu-util
            cargo-binutils
          ];
        };
      }
    );
}
